package com.capstone6.doconnect.repositry;

import org.springframework.data.jpa.repository.JpaRepository;

import com.capstone6.doconnect.model.AdminDetails;


public interface AdminRepositry extends JpaRepository<AdminDetails, Integer> {
	
	public AdminDetails findByEmailAndPassword(String email,String password);
	public boolean existsByEmail(String email);

	
}
