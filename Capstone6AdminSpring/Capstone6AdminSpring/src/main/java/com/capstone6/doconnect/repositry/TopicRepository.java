package com.capstone6.doconnect.repositry;

import org.springframework.data.jpa.repository.JpaRepository;

import com.capstone6.doconnect.model.Topic;

public interface TopicRepository extends JpaRepository<Topic, Integer>{

}
