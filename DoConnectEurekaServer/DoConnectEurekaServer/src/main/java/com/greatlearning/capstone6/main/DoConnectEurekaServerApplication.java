package com.greatlearning.capstone6.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class DoConnectEurekaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(DoConnectEurekaServerApplication.class, args);
		System.out.println("server running on port no : 8761");
	}

}
