Creating database ->
create database doconnect6;

use doconnect6;

create table admin(
   adminid int NOT NULL AUTO_INCREMENT,
   adminname varchar(255) NOT NULL,
   email varchar(225) NOT NULL,
   password varchar(45) NOT NULL,
   PRIMARY KEY (adminid)
);

insert into admin values(1,'Capstone6','doconnectcapstone@gmail.com','akrhkyohoszgjuom');

select *from admin;
------------------------------------------>

create table users(
   userid int NOT NULL AUTO_INCREMENT,
   email varchar(45) NOT NULL,
   username varchar(45) NOT NULL,
   password varchar(45) NOT NULL,
   primary key (userid)
);

insert into users values(1,'usha@gmail.com','Usha','12345');
insert into users values(2,'ajay@gmail.com','Ajay','123456');

select *from users;
----------------------------------------->

create table topic(
  topicid int NOT NULL AUTO_INCREMENT,
  cetegory varchar(255) NOT NULL,
  PRIMARY KEY (topicid)
);

insert into topic values(1,'angular');
insert into topic values(2,'maths');
insert into topic values(3,'wmc');
insert into topic values(4,'java');

select *from topic;
----------------------------------------->
create table question(
   id int NOT NULL AUTO_INCREMENT,
   qstatus int NOT NULL,
   questionimage text,
   questionname text NOT NULL,
   topic_topicid int NOT NULL,
   user_userid int NOT NULL,
   PRIMARY KEY (id),
   FOREIGN KEY (user_userid) REFERENCES users (userid),
   FOREIGN KEY (topic_topicid) REFERENCES topic (topicid)
);

insert into question values(1,1,"image","angular",1,1);
insert into question values(2,1,"image","maths",2,1);

select *from question;
------------------------------------------->
create table answer(
   id int NOT NULL AUTO_INCREMENT,
   answerimage text,
   answername text NOT NULL,
   astatus int NOT NULL,
   question_id int NOT NULL,
   user_userid int NOT NULL,
   PRIMARY KEY (id),
   FOREIGN KEY (question_id) REFERENCES question (id),
   FOREIGN KEY (user_userid) REFERENCES users (userid)
);
 
insert into answer values(1,"image","java",1,1,1);

select *from answer;

----------------------------------------------------------------EOF-------------------------------------------------------------------------------------