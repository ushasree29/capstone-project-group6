package com.capstone6.doconnect.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capstone6.doconnect.model.UserDetails;
import com.capstone6.doconnect.repositry.UserRepositry;

@Service
public class UserService {

	@Autowired
	private UserRepositry userRepositry;

	public UserDetails saveUser(UserDetails user) {
		return this.userRepositry.save(user);
	}

	public UserDetails loginUser(String email, String password) {
		return userRepositry.findByEmailAndPassword(email, password);
	}

	public Object getUserByEmail(String email) {
		
		return userRepositry.findByEmail(email);
	}


	
	
	

}
